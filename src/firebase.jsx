import firebase from 'firebase/app';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyAznz4tp5s_PXIPFWweNXNbK074mzPYHiA',
  authDomain: 'bdnike-6f2df.firebaseapp.com',
  projectId: 'bdnike-6f2df',
  storageBucket: 'bdnike-6f2df.appspot.com',
  messagingSenderId: '901032783509',
  appId: '1:901032783509:web:8c783677cdfc546b06759c',
  measurementId: 'G-KC47Z60SDM',
};

firebase.initializeApp(firebaseConfig);

export { firebase };
