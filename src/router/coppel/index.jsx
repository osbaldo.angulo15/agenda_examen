import React, { Fragment } from 'react';
import {
  Button, Grid, TextField, Typography,
} from '@mui/material';
import _ from 'lodash';
import { keyValidation } from './helper';
import Searcher from '../../components/Searcher';
import useAgenda from './hooks/useAgenda';

function Agenda() {
  const {
    registro, handleSetData, handleAddData, datos, valorBuscador, handleBuscador, datosFiltrados,
  } = useAgenda();
  return (
    <Fragment>
      <Grid container style={{ margin: '34px' }} spacing={2}>
        <Grid container item spacing={2} alignItems="center">
          <Grid item xs={4}><TextField label="Nombre" value={registro.nombre} fullWidth onKeyPress={(e) => keyValidation(e, 1)} onChange={(e) => handleSetData('nombre', e.target.value)} /></Grid>
          <Grid item xs={4}><TextField label="Teléfono" value={registro.telefono} fullWidth onKeyPress={(e) => keyValidation(e, 2)} onChange={(e) => handleSetData('telefono', e.target.value)} /></Grid>
          <Grid item xs={2}>
            <Button fullWidth variant="contained" onClick={handleAddData} disabled={!(registro.nombre !== '' && registro.telefono.length === 10)}>Guardar</Button>
          </Grid>
        </Grid>
        <Grid container item spacing={2} alignItems="center">
          <Grid item xs={4}><Searcher label="Nombre" items={datos.dataAgenda} property="nombre" filteredData={handleBuscador} fullWidth value={valorBuscador} /></Grid>
        </Grid>
      </Grid>
      <Grid container style={{ margin: '34px' }}>
        <Grid container>
          <Grid item xs={6} style={{ padding: '16px', border: '1px solid gray' }}><Typography><strong>Nombre</strong></Typography></Grid>
          <Grid item xs={6} style={{ padding: '16px', border: '1px solid gray' }}><Typography><strong>Teléfono</strong></Typography></Grid>
        </Grid>
        {_.orderBy(datosFiltrados, ['nombre'], ['asc']).map((usuario) => (
          <Grid container key={usuario.id}>
            <Grid item xs={6} style={{ padding: '16px', border: '1px solid gray' }}><Typography>{usuario.nombre}</Typography></Grid>
            <Grid item xs={6} style={{ padding: '16px', border: '1px solid gray' }}><Typography>{usuario.telefono}</Typography></Grid>
          </Grid>
        ))}
      </Grid>
    </Fragment>
  );
}

export default Agenda;
