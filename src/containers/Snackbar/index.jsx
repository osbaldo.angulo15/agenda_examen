// @flow
import React, { memo } from 'react';
import { Snackbar, Alert, Slide } from '@mui/material';
import { useSelector, useDispatch } from 'react-redux';
import { setSnackbar } from '../../redux/principalDucks';

const SnackBar = () => {
  const principal = useSelector((store) => store.principal);
  const dispatch = useDispatch();
  return (
    <Snackbar open={principal.snackbar} autoHideDuration={3000} TransitionComponent={Slide} anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }} onClose={() => dispatch(setSnackbar(null))}>
      <Alert onClose={() => dispatch(setSnackbar(null))} severity={principal.snackbar?.severity} variant="filled">
        {principal.snackbar?.mensaje.message}
      </Alert>
    </Snackbar>
  );
};

export default memo(SnackBar);
