import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { firebase } from '../../../firebase';
import { setSnackbar } from '../../../redux/principalDucks';

export default function useAgenda() {
  const [registro, setRegistro] = useState({ nombre: '', telefono: '' });
  const [datos, setDatos] = useState({ dataAgenda: [] });
  const [datosFiltrados, setDatosFiltrados] = useState([]);
  const [valorBuscador, setValorBuscador] = useState('');
  const db = firebase.firestore();
  const dispatch = useDispatch();

  useEffect(() => {
    const obtenerDatos = async () => {
      try {
        const aux = { ...datos };
        const dataAgenda = await db.collection('agenda').get();
        aux.dataAgenda = (dataAgenda.docs.map((usuario) => ({ id: usuario.id, ...usuario.data() })));
        setDatos(aux);
        setDatosFiltrados(aux.dataAgenda);
      } catch (error) {
        dispatch(setSnackbar({ severity: 'error', mensaje: error.mensaje }));
      }
    };
    obtenerDatos();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [registro]);

  function handleSetData(type, value) {
    const aux = { ...registro };
    aux[type] = value;
    setRegistro(aux);
  }

  function handleAddData() {
    db.collection('agenda').add(registro).then(() => {
      dispatch(setSnackbar({ severity: 'success', mensaje: { message: 'Agregado correctamente' } }));
      setRegistro({ nombre: '', telefono: '' });
    });
  }

  function handleBuscador(items, value) {
    setDatosFiltrados(items);
    setValorBuscador(value);
  }

  return {
    registro, handleSetData, handleAddData, datos, valorBuscador, handleBuscador, datosFiltrados,
  };
}
