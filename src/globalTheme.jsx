export const GlobalTheme = {
  palette: {
    primary: {
      main: '#000',
      contrastText: '#FFFFFF',
    },
  },
};
