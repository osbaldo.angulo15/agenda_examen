import React, { Fragment, memo } from 'react';
import PropTypes from 'prop-types';
import { InputAdornment, IconButton, TextField } from '@mui/material';
import BackspaceOutlinedIcon from '@mui/icons-material/BackspaceOutlined';

function Seacher({
  placeholder, property, filteredData, items, fullWidth, value,
}) {
  const onSearch = (e) => {
    const matcher = new RegExp(e.target.value, 'gi');
    if (e.target.value.trim() === '') filteredData(items);
    filteredData(items.filter((n) => n[property].match(matcher)), e.target.value);
  };
  return (
    <Fragment>
      <TextField
        label="Busqueda"
        value={value}
        endAdornment={value ? (
          <InputAdornment position="end">
            <IconButton onClick={() => filteredData([], '')}><BackspaceOutlinedIcon color="primary" /></IconButton>
          </InputAdornment>
        ) : null}
        onChange={onSearch}
        placeholder={placeholder}
        fullWidth={fullWidth}
      />
    </Fragment>
  );
}

Seacher.propTypes = {
  placeholder: PropTypes.string,
  property: PropTypes.string,
  filteredData: () => {},
  items: PropTypes.array,
  fullWidth: PropTypes.bool,
  value: PropTypes.string,
};

Seacher.propTypes = {
  placeholder: '',
  property: '',
  filteredData: [],
  items: [],
  fullWidth: false,
};

export default memo(Seacher);
