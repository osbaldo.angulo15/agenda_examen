const dataInicial = {
  snackbar: null,
  blur: false,
};

const ACTIVAR_SNACKBAR = 'ACTIVAR_SNACKBAR';
const BLUR = 'BLUR';

export default function principalReducer(state = dataInicial, action) {
  switch (action.type) {
    case ACTIVAR_SNACKBAR:
      return { ...state, snackbar: action.payload.data };
    case BLUR:
      return { ...state, blur: action.payload.data };
    default:
      return state;
  }
}

export const setSnackbar = (data) => (dispatch) => {
  dispatch({ type: ACTIVAR_SNACKBAR, payload: { data } });
};

export const setBlur = (data) => (dispatch) => {
  dispatch({ type: BLUR, payload: { data } });
};
