export const keyValidation = (e, tipo) => {
  const key = e.keyCode || e.which;
  const teclado = String.fromCharCode(key).toLowerCase();
  const letras = 'áéíóúüabcdefghijklmnñopqrstuvwxyz. ';
  const numeros = '0123456789';
  const otros = ' ';
  const validos = tipo === 1 ? letras + otros : tipo === 2 ? numeros : letras + numeros;
  if (validos.indexOf(teclado) === -1) {
    e.preventDefault();
  }
};
