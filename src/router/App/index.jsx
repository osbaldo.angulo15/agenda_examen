import React from 'react';
import {
  BrowserRouter as Router, Switch, Route,
} from 'react-router-dom';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Provider } from 'react-redux';
import moment from 'moment';
import { GlobalTheme } from '../../globalTheme';
import generateStore from '../../redux/store';
import Snackbar from '../../containers/Snackbar';
import 'moment/locale/es';
import Agenda from '../coppel';

moment.locale('ES');

export default function App() {
  const store = generateStore();

  return (
    <Provider store={store}>
      <ThemeProvider theme={createTheme(GlobalTheme)}>
        <Router>
          <Switch>
            <Route path="/" exact><Agenda /></Route>
          </Switch>
          <Snackbar />
        </Router>
      </ThemeProvider>
    </Provider>
  );
}
